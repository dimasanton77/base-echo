package configs

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

var DB *gorm.DB

func init() {
	// Load environment variables from .env
	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env file")
		os.Exit(1)
	}

	// Get database configurations from environment variables
	dbConfig := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"),
	)

	// Open a connection to the PostgreSQL database
	db, err := gorm.Open("postgres", dbConfig)
	if err != nil {
		fmt.Println("Error connecting to the database:", err) // Print the error
		os.Exit(1)
	}

	// Set the database connection to use singular table names (optional)
	db.SingularTable(true)

	// Print success message
	fmt.Println("Success connect to the database")

	DB = db
}
