package main

import (
	"base-project/configs"
	"base-project/routes"
	"base-project/seeders"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Inisialisasi echo
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Seeder
	seeders.SeedRoles()

	// Load environment variables from .env file
	if err := godotenv.Load(); err != nil {
		e.Logger.Fatal("Error loading .env file")
	}

	// Get the port from the environment variables, default to 1323 if not set
	port := os.Getenv("PORT")
	if port == "" {
		port = "1323"
	}

	// Middleware untuk logging ke file app.log
	logFile, err := os.Create("logs/app.log")
	if err != nil {
		log.Fatal(err)
	}
	defer logFile.Close()

	// Gunakan middleware.LoggerWithConfig dan wrap dengan middleware.Logger
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: io.MultiWriter(logFile, os.Stdout),
	}))

	// Middleware Recover untuk menangani panic
	e.Use(middleware.Recover())

	// Middleware untuk menangani error dan logging ke file error.log
	errorFile, err := os.Create("errors/error.log")
	if err != nil {
		log.Fatal(err)
	}
	defer errorFile.Close()

	// Gunakan middleware.LoggerWithConfig dan wrap dengan middleware.Logger
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: io.MultiWriter(errorFile, os.Stderr),
	}))

	// Middleware untuk menangani error HTTP dan logging ke file error.log
	e.HTTPErrorHandler = customHTTPErrorHandler

	// Setup routes
	routes.SetupRoutes(e)

	e.Logger.Fatal(e.Start(":" + port))

	defer configs.DB.Close()
}

// Custom HTTP error handler untuk logging error ke file error.log
func customHTTPErrorHandler(err error, c echo.Context) {
	e := c.Echo()
	code := http.StatusInternalServerError

	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}

	// Log error menggunakan echo.Logger
	e.Logger.Error(err)

	// Mengirimkan response error
	c.JSON(code, map[string]interface{}{"error": err.Error()})
}
