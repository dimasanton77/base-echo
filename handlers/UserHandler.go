package handlers

import (
	"base-project/models"
	"net/http"
	"sync"
	"time"

	"base-project/configs"

	"github.com/golang-jwt/jwt/v5"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

var user = models.User{}
var (
	users = make(map[int]*models.User)
	seq   = 1
	mu    sync.Mutex
)

// Register handles user registration
func Register(c echo.Context) error {
	mu.Lock()
	defer mu.Unlock()

	db := configs.DB
	u := &models.UserBind{}

	// Mulai transaksi
	tx := db.Begin()

	if err := c.Bind(u); err != nil {
		// Rollback transaksi jika terjadi error pada binding
		tx.Rollback()
		return err
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		// Rollback transaksi jika terjadi error pada hashing password
		tx.Rollback()
		return echo.NewHTTPError(http.StatusInternalServerError, "Failed to hash password")
	}
	u.Password = string(hashedPassword)

	// Simpan user ke database
	if err := tx.Create(u).Error; err != nil {
		// Rollback transaksi jika terjadi error pada penyimpanan ke database
		tx.Rollback()
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	// Commit transaksi jika semuanya berhasil
	tx.Commit()

	return c.JSON(http.StatusCreated, u)
}

func Login(c echo.Context) error {
	// Mendapatkan input pengguna
	inputUser := new(models.User)
	if err := c.Bind(inputUser); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	// Validasi input pengguna (Anda dapat menambahkan validasi lebih lanjut sesuai kebutuhan)
	if inputUser.Email == "" || inputUser.Password == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Email dan password diperlukan")
	}

	// Memeriksa kredensial pengguna di database
	db := configs.DB
	storedUser := new(models.User)

	err := db.Where("email = ?", inputUser.Email).First(storedUser).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return echo.NewHTTPError(http.StatusUnauthorized, "Email atau password tidak valid")
		}
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	// Membandingkan password yang di-hash
	err = bcrypt.CompareHashAndPassword([]byte(storedUser.Password), []byte(inputUser.Password))
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Email atau password tidak valid")
	}

	// Menetapkan klaim kustom
	claims := &models.JwtCustomClaims{
		storedUser.ID,
		storedUser.RoleID,
		storedUser.Email,
		storedUser.Name,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 72)),
		},
	}

	// Membuat token dengan klaim
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Menghasilkan token terenkripsi dan mengirimkannya sebagai respons.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
}

func GetUser(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*models.JwtCustomClaims)
	return c.String(http.StatusOK, claims.Name)
}

func GetAllUser(c echo.Context) error {
	db := configs.DB

	// Inisialisasi model User
	var users []models.User

	// Mengambil semua data user
	if err := db.Find(&users).Error; err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, users)

}

func GetDetailUser(c echo.Context) error {
	db := configs.DB

	// Dapatkan ID pengguna dari parameter permintaan
	userID := c.Param("id")

	// Inisialisasi model User
	var user models.User

	// Temukan pengguna berdasarkan ID
	if err := db.Preload("Role").First(&user, userID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return echo.NewHTTPError(http.StatusNotFound, "User not found")
		}
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, user)
}

func EditUser(c echo.Context) error {
	db := configs.DB

	// Mulai transaksi
	tx := db.Begin()

	// Dapatkan ID pengguna dari parameter permintaan
	userID := c.Param("id")

	// Inisialisasi model User untuk menyimpan data pengguna yang ada
	var existingUser models.User

	// Temukan pengguna berdasarkan ID
	if err := db.First(&existingUser, userID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return echo.NewHTTPError(http.StatusNotFound, "Pengguna tidak ditemukan")
		}
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	// Dapatkan input pengguna untuk pembaruan
	updatedUser := new(models.UserBind)
	if err := c.Bind(updatedUser); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	// Perbarui data pengguna yang ada
	tx.Model(&existingUser).Updates(updatedUser)

	// Commit transaksi
	if err := tx.Commit().Error; err != nil {
		// Tangani kesalahan commit
		return echo.NewHTTPError(http.StatusInternalServerError, "Gagal menyimpan perubahan")
	}

	return c.JSON(http.StatusOK, updatedUser)
}

func DeleteUser(c echo.Context) error {
	db := configs.DB

	// Dapatkan ID pengguna dari parameter permintaan
	userID := c.Param("id")

	// Mulai transaksi
	tx := db.Begin()

	// Inisialisasi model User untuk menyimpan data pengguna yang akan dihapus
	var userToDelete models.User

	// Temukan pengguna berdasarkan ID
	if err := tx.First(&userToDelete, userID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			// Rollback transaksi jika pengguna tidak ditemukan
			tx.Rollback()
			return echo.NewHTTPError(http.StatusNotFound, "Pengguna tidak ditemukan")
		}
		// Rollback transaksi jika terjadi kesalahan lainnya
		tx.Rollback()
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	// Hapus pengguna dari database
	if err := tx.Delete(&userToDelete).Error; err != nil {
		// Rollback transaksi jika terjadi kesalahan penghapusan
		tx.Rollback()
		return echo.NewHTTPError(http.StatusInternalServerError, "Gagal menghapus pengguna")
	}

	// Commit transaksi
	if err := tx.Commit().Error; err != nil {
		// Tangani kesalahan commit
		return echo.NewHTTPError(http.StatusInternalServerError, "Gagal menyimpan perubahan")
	}

	return c.JSON(http.StatusOK, echo.Map{"message": "Pengguna berhasil dihapus"})
}

func HardDeleteUser(c echo.Context) error {
	db := configs.DB

	// Dapatkan ID pengguna dari parameter permintaan
	userID := c.Param("id")

	// Inisialisasi model User untuk menyimpan data pengguna yang akan dihapus
	var userToDelete models.User

	// Temukan pengguna berdasarkan ID
	if err := db.First(&userToDelete, userID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return echo.NewHTTPError(http.StatusNotFound, "Pengguna tidak ditemukan")
		}
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	// Hapus pengguna dari database (hard delete)
	if err := db.Unscoped().Delete(&userToDelete).Error; err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Gagal menghapus pengguna")
	}

	return c.JSON(http.StatusOK, echo.Map{"message": "Pengguna berhasil dihapus secara permanen"})
}
