// utils/logger.go
package utils

import (
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

var Logger *logrus.Logger

func init() {
	Logger = NewLogger()
}

func NewLogger() *logrus.Logger {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})

	return logger
}

func ErrorLogger() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if err := next(c); err != nil {
				Logger.WithFields(logrus.Fields{
					"error": err,
				}).Error("Unhandled error")
			}
			return nil
		}
	}
}
