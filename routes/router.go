package routes

import (
	"base-project/handlers"
	"base-project/models"

	"github.com/golang-jwt/jwt/v5"
	echojwt "github.com/labstack/echo-jwt/v4"

	"github.com/labstack/echo/v4"
)

func SetupRoutes(e *echo.Echo) {

	// Routes
	e.POST("/register", handlers.Register)
	e.POST("/login", handlers.Login)

	r := e.Group("/api")

	// Configure middleware with the custom claims type
	config := echojwt.Config{
		NewClaimsFunc: func(c echo.Context) jwt.Claims {
			return new(models.JwtCustomClaims)
		},
		SigningKey: []byte("secret"),
	}
	r.Use(echojwt.WithConfig(config))
	r.GET("/get-user", handlers.GetUser)
	r.GET("/users", handlers.GetAllUser)
	r.PUT("/users/:id", handlers.EditUser)
	r.DELETE("/users/:id", handlers.DeleteUser)
	r.DELETE("/users-delete/:id", handlers.HardDeleteUser)
	r.GET("/users/:id", handlers.GetDetailUser)

	// // Secured route (requires token)
	// secured := e.Group("/secured")
	// secured.Use(auth.JWTMiddleware())
	// secured.GET("/user", handlers.GetUser)

	// e.GET("/users/:id", handlers.GetUser)

}
