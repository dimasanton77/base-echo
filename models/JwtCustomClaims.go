package models

import (
	"github.com/golang-jwt/jwt/v5"
)

type JwtCustomClaims struct {
	ID     int    `json:"-"`
	RoleID int    `json:"role_id"`
	Email  string `json:"email"`
	Name   string `json:"name"`
	jwt.RegisteredClaims
}
