package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID       int    `gorm:"primaryKey" json:"-"`
	RoleID   int    `json:"role_id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Role     Role   `gorm:"foreignkey:RoleID" json:"role"`
}

type UserBind struct {
	gorm.Model
	RoleID   int    `json:"role_id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

func (u *User) TableName() string {
	return "users"
}

func (u *UserBind) TableName() string {
	return "users"
}
