package models

import (
	"gorm.io/gorm"
)

type Role struct {
	gorm.Model
	ID   int    `gorm:"primaryKey" json:"-"`
	Name string `json:"name"`
}

func (u *Role) TableName() string {
	return "roles"
}
