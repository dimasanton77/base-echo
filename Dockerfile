# Gunakan image Golang sebagai base image
FROM golang:1.22

# Set direktori kerja
WORKDIR /app

# Salin go.mod dan go.sum ke dalam image
COPY go.mod go.sum ./

# Download dependensi dengan go mod
RUN go mod download

RUN go get -u github.com/golang-migrate/migrate/v4/cmd/migrate

# RUN go get -u github.com/cosmtrek/air


# Salin seluruh proyek ke dalam image
COPY . .

RUN go build -o base_echo .

# RUN migrate -path migrations -database "postgres://postgres:postgre@host.docker.internal:5432/base_echo?sslmode=disable" up

# Expose port yang akan digunakan oleh aplikasi
EXPOSE 3000

CMD ["./base_echo"]
